# Back-office du projet MSPR CI 
# Python 3 <img alt="" src="resources/python.svg" width="32px" height="32px"> 

# Assurez vous de bien avoir Python 3 d'installé ! 
```
https://www.python.org/download/releases/3.0/
```
# Sonarcloud métriques
<a href="https://sonarcloud.io/component_measures?id=mspr-clsv_MSPR-continus-integration&metric=coverage&view=list">
    <img alt="" src="https://sonarcloud.io/api/project_badges/measure?project= mspr-clsv_MSPR-continus-integration&metric=coverage">
</a>
<a href="https://sonarcloud.io/api/project_badges/measure?project=mspr-clsv_MSPR-continus-integration&metric=bugs">
    <img alt="" src="https://sonarcloud.io/project/issues?id=mspr-clsv_MSPR-continus-integration&resolved=false&types=BUG">
</a>
<a href="https://sonarcloud.io/api/project_badges/measure?project=mspr-clsv_MSPR-continus-integration&metric=vulnerabilities">
    <img alt="" src="https://sonarcloud.io/api/project_badges/measure?project=mspr-clsv_MSPR-continus-integration&metric=vulnerabilities">
</a>
<a href="https://sonarcloud.io/project/issues?id=mspr-clsv_MSPR-continus-integration&resolved=false&types=CODE_SMELL">
    <img alt="" src="https://sonarcloud.io/api/project_badges/measure?project=mspr-clsv_MSPR-continus-integration&metric=code_smells">
</a>
<a href="https://sonarcloud.io/component_measures?id=mspr-clsv_MSPR-continus-integration&metric=new_duplicated_lines">
    <img alt="" src="https://sonarcloud.io/api/project_badges/measure?project=mspr-clsv_MSPR-continus-integration&metric=duplicated_lines_density">
</a>
<a href="https://sonarcloud.io/component_measures?id=mspr-clsv_MSPR-continus-integration&metric=sqale_index&view=list">
    <img alt="" src="https://sonarcloud.io/api/project_badges/measure?project=mspr-clsv_MSPR-continus-integration&metric=sqale_index">
</a>

# Installations des dépendances 
```
pip3 install -r requirements.txt
```

# Migration de la base de données
```
python3 manage.py makemigrations app && python3 manage.py migrate app
python3 manage.py makemigrations && python3 manage.py migrate
```
# Déploiement de l'API
```
python3 manage.py runserver
```

# Déploiement de l'API sur un port spécifique
```
python3 manage.py runserver <port>
```

# Lancement des tests
```
coverage run --source='.' manage.py test app.functionnaltests app.tests
```

# Obtenir le code coverage dans la console
```
coverage report
```

# Exporter le rapport du code coverage en format XML
```
coverage xml -o <nom_fichier>
```









