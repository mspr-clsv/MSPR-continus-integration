from django.contrib import admin
from app.models import Client, Product, Purchase

# Register your models here.
admin.site.register(Client)
admin.site.register(Product)
admin.site.register(Purchase)