from django.test import TestCase
from app.models import Client, Product, Purchase
from django.urls import reverse


class ClientTestCase(TestCase):

    def setUp(self):
        client = Client(1,"benjamin","vitis")
        client.save()
        product = Product(1,"pc",600)
        product.save() 

    def testClientBill(self):
        client = Client.objects.get(firstname="benjamin")
        product = Product.objects.get(name="pc")
        purchase = Purchase(client=client,product=product,amount=2)
        purchase.save()
        self.assertEquals(product.price,600)
        self.assertEquals(client.lastname,"vitis")
        self.assertEquals(client.bill(),1200)





# Create your tests here.
