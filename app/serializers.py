from rest_framework import serializers
from app.models import Client, Product, Purchase


class ClientSerializer(serializers.ModelSerializer):
    firstname = serializers.CharField()
    lastname = serializers.CharField()
    bill = serializers.SerializerMethodField()

    def bill(self, client):
        return client.bill()

    class Meta:
        model = Client
        fields = ('id', 'firstname', 'lastname', 'bill')


class ProductSerializer(serializers.ModelSerializer):
    name = serializers.CharField()
    price = serializers.DecimalField(max_digits=20,  decimal_places=2)

    class Meta:
        model = Product
        fields = ('id', 'name', 'price')


class PurchaseSerializer(serializers.ModelSerializer):
    product = ProductSerializer()
    amount = serializers.IntegerField()

    class Meta:
        model = Purchase
        fields = ('id', 'client', 'product', 'amount')