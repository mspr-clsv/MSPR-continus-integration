from django.db import models


class Product(models.Model):
    name = models.CharField(max_length=120, null=False)
    price = models.DecimalField(max_digits=20,  decimal_places=2, null=False)


class Client(models.Model):
    firstname = models.CharField(max_length=120, null=False)
    lastname = models.CharField(max_length=120, null=False)

    def bill(self):
        total = 0.0
        purchase = Purchase.objects.filter(client=self).select_related('product')
        for p in purchase:
            total += float(p.product.price) * p.amount
        return round(total, 2)


class Purchase(models.Model):
    client = models.ForeignKey(Client, on_delete=models.CASCADE, null=False)
    product = models.ForeignKey(Product, on_delete=models.CASCADE, null=False)
    amount = models.IntegerField(default=1)