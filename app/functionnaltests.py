from rest_framework.test import RequestsClient
from django.test import TestCase
import json

class ClientFunctionnalTest(TestCase):

    def test_get_nominal_case(self):
        client = RequestsClient()
        response = client.get('http://testserver/client')
        assert response.status_code == 200
        dictionary = json.loads(response._content.decode('utf-8'))
        assert len(dictionary) == 0

    def test_post_nominal_case(self):
        client = RequestsClient()
        response = client.post('http://testserver/client/', json={'firstname': 'Hoang','lastname': 'Cornu'})
        assert response.status_code == 201
        dictionary = json.loads(response._content.decode('utf-8'))
        assert dictionary['firstname'] == 'Hoang'
        assert dictionary['lastname'] == 'Cornu'

        response = client.get('http://testserver/client')
        assert response.status_code == 200
        dictionary = json.loads(response._content.decode('utf-8'))
        assert len(dictionary) == 1
        assert dictionary[0]['id'] == 1
        assert dictionary[0]['firstname'] == 'Hoang'
        assert dictionary[0]['lastname'] == 'Cornu'
        assert dictionary[0]['bill'] == 0.0

    def test_put_nominal_case(self):
        client = RequestsClient()
        response = client.post('http://testserver/client/', json={'firstname': 'Hoang','lastname': 'Cornu'})
        assert response.status_code == 201
        dictionary = json.loads(response._content.decode('utf-8'))
        assert dictionary['firstname'] == 'Hoang'
        assert dictionary['lastname'] == 'Cornu'

        response = client.put('http://testserver/client/1/', json={'firstname': 'firstname','lastname': 'Cornu'})
        assert response.status_code == 200
        dictionary = json.loads(response._content.decode('utf-8'))
        assert dictionary['firstname'] == 'firstname'
        assert dictionary['lastname'] == 'Cornu'

        response = client.get('http://testserver/client')
        assert response.status_code == 200
        dictionary = json.loads(response._content.decode('utf-8'))
        assert len(dictionary) == 1
        assert dictionary[0]['id'] == 1
        assert dictionary[0]['firstname'] == 'firstname'
        assert dictionary[0]['lastname'] == 'Cornu'
        assert dictionary[0]['bill'] == 0.0

    def test_delete_nominal_case(self):
        client = RequestsClient()
        response = client.post('http://testserver/client/', json={'firstname': 'Hoang','lastname': 'Cornu'})
        assert response.status_code == 201
        dictionary = json.loads(response._content.decode('utf-8'))
        assert dictionary['firstname'] == 'Hoang'
        assert dictionary['lastname'] == 'Cornu'

        response = client.delete('http://testserver/client/1/')
        assert response.status_code == 204

        response = client.get('http://testserver/client')
        assert response.status_code == 200
        dictionary = json.loads(response._content.decode('utf-8'))
        assert len(dictionary) == 0   

class ProductFunctionnalTest(TestCase):
    def test_get_nominal_case(self):
        client = RequestsClient()
        response = client.get('http://testserver/product')
        assert response.status_code == 200
        dictionary = json.loads(response._content.decode('utf-8'))
        assert len(dictionary) == 0

    def test_post_nominal_case(self):
        client = RequestsClient()
        response = client.post('http://testserver/product/', json={'name': 'Camembert','price': '1.50'})
        assert response.status_code == 201
        dictionary = json.loads(response._content.decode('utf-8'))
        assert dictionary['name'] == 'Camembert'
        assert dictionary['price'] == '1.50'

        response = client.get('http://testserver/product')
        assert response.status_code == 200
        dictionary = json.loads(response._content.decode('utf-8'))
        assert len(dictionary) == 1
        assert dictionary[0]['id'] == 1
        assert dictionary[0]['name'] == 'Camembert'
        assert dictionary[0]['price'] == '1.50'

    def test_put_nominal_case(self):
        client = RequestsClient()
        response = client.post('http://testserver/product/', json={'name': 'Camembert','price': '1.50'})
        assert response.status_code == 201
        dictionary = json.loads(response._content.decode('utf-8'))
        assert dictionary['name'] == 'Camembert'
        assert dictionary['price'] == '1.50'

        response = client.put('http://testserver/product/1/', json={'name': 'Camembert','price': '2'})
        assert response.status_code == 200
        dictionary = json.loads(response._content.decode('utf-8'))
        assert dictionary['name'] == 'Camembert'
        assert dictionary['price'] == '2.00'

        response = client.get('http://testserver/product')
        assert response.status_code == 200
        dictionary = json.loads(response._content.decode('utf-8'))
        assert len(dictionary) == 1
        assert dictionary[0]['id'] == 1
        assert dictionary[0]['name'] == 'Camembert'
        assert dictionary[0]['price'] == '2.00'

    def test_delete_nominal_case(self):
        client = RequestsClient()
        response = client.post('http://testserver/product/', json={'name': 'Camembert','price': '1.50'})
        assert response.status_code == 201
        dictionary = json.loads(response._content.decode('utf-8'))
        assert dictionary['name'] == 'Camembert'
        assert dictionary['price'] == '1.50'

        response = client.delete('http://testserver/product/1/')
        assert response.status_code == 204

        response = client.get('http://testserver/product')
        assert response.status_code == 200
        dictionary = json.loads(response._content.decode('utf-8'))
        assert len(dictionary) == 0    

class PurchaseFunctionnalTest(TestCase):

    def test_get_nominal_case(self):
        client = RequestsClient()
        response = client.get('http://testserver/purchase')
        assert response.status_code == 200
        dictionary = json.loads(response._content.decode('utf-8'))
        assert len(dictionary) == 0

    def test_get_nominal_case_by_client(self):
        client = RequestsClient()
        response = client.get('http://testserver/purchase?client/1')
        assert response.status_code == 200
        dictionary = json.loads(response._content.decode('utf-8'))
        assert len(dictionary) == 0

    def test_post_nominal_case(self):
        client = RequestsClient()
        response = client.post('http://testserver/client/', json={'firstname': 'Hoang','lastname': 'Cornu'})
        assert response.status_code == 201
        dictionary = json.loads(response._content.decode('utf-8'))
        assert dictionary['id'] == 1
        assert dictionary['firstname'] == 'Hoang'
        assert dictionary['lastname'] == 'Cornu'
        assert dictionary['bill'] == 0.0

        response = client.post('http://testserver/product/', json={'name': 'Camembert','price': '1.50'})
        assert response.status_code == 201
        dictionary = json.loads(response._content.decode('utf-8'))
        assert dictionary['name'] == 'Camembert'
        assert dictionary['price'] == '1.50'

        response = client.post('http://testserver/purchase/', json={'client': 1, 'product': 1, 'amount': 1})
        assert response.status_code == 201
        dictionary = json.loads(response._content.decode('utf-8'))
        assert dictionary['id'] == 1
        assert dictionary['client'] == 1
        assert dictionary['product']['id'] == 1
        assert dictionary['amount'] == 1

        response = client.get('http://testserver/purchase')
        assert response.status_code == 200
        dictionary = json.loads(response._content.decode('utf-8'))
        assert len(dictionary) == 1
        assert dictionary[0]['id'] == 1
        assert dictionary[0]['client'] == 1
        assert dictionary[0]['product']['id'] == 1
        assert dictionary[0]['amount'] == 1

    def test_post_nominal_case_by_client(self):
        client = RequestsClient()
        response = client.post('http://testserver/client/', json={'firstname': 'Hoang','lastname': 'Cornu'})
        assert response.status_code == 201
        dictionary = json.loads(response._content.decode('utf-8'))
        assert dictionary['id'] == 1
        assert dictionary['firstname'] == 'Hoang'
        assert dictionary['lastname'] == 'Cornu'
        assert dictionary['bill'] == 0.0

        response = client.post('http://testserver/product/', json={'name': 'Camembert','price': '1.50'})
        assert response.status_code == 201
        dictionary = json.loads(response._content.decode('utf-8'))
        assert dictionary['name'] == 'Camembert'
        assert dictionary['price'] == '1.50'

        response = client.post('http://testserver/purchase/', json={'client': 1, 'product': 1, 'amount': 1})
        assert response.status_code == 201
        dictionary = json.loads(response._content.decode('utf-8'))
        assert dictionary['id'] == 1
        assert dictionary['client'] == 1
        assert dictionary['product']['id'] == 1
        assert dictionary['amount'] == 1

        response = client.get('http://testserver/purchase?client=1')
        assert response.status_code == 200
        dictionary = json.loads(response._content.decode('utf-8'))
        assert len(dictionary) == 1
        assert dictionary[0]['id'] == 1
        assert dictionary[0]['client'] == 1
        assert dictionary[0]['product']['id'] == 1
        assert dictionary[0]['amount'] == 1

        response = client.get('http://testserver/purchase?client=2')
        assert response.status_code == 200
        dictionary = json.loads(response._content.decode('utf-8'))
        assert len(dictionary) == 0

    def test_delete_nominal_case(self):
        client = RequestsClient()
        response = client.post('http://testserver/client/', json={'firstname': 'Hoang','lastname': 'Cornu'})
        assert response.status_code == 201
        dictionary = json.loads(response._content.decode('utf-8'))
        assert dictionary['id'] == 1
        assert dictionary['firstname'] == 'Hoang'
        assert dictionary['lastname'] == 'Cornu'
        assert dictionary['bill'] == 0.0

        response = client.post('http://testserver/product/', json={'name': 'Camembert','price': '1.50'})
        assert response.status_code == 201
        dictionary = json.loads(response._content.decode('utf-8'))
        assert dictionary['name'] == 'Camembert'
        assert dictionary['price'] == '1.50'

        response = client.post('http://testserver/purchase/', json={'client': 1, 'product': 1, 'amount': 1})
        assert response.status_code == 201
        dictionary = json.loads(response._content.decode('utf-8'))
        assert dictionary['id'] == 1
        assert dictionary['client'] == 1
        assert dictionary['product']['id'] == 1
        assert dictionary['amount'] == 1

        response = client.delete('http://testserver/purchase/1/')
        assert response.status_code == 204

        response = client.get('http://testserver/purchase')
        assert response.status_code == 200
        dictionary = json.loads(response._content.decode('utf-8'))
        assert len(dictionary) == 0    
 