from django.shortcuts import render
from rest_framework import viewsets, views, status
from rest_framework.response import Response
from app.serializers import ClientSerializer, ProductSerializer, PurchaseSerializer
from app.models import Client, Product, Purchase
from django.forms.models import model_to_dict

class ClientViewSet(viewsets.ModelViewSet):
    serializer_class = ClientSerializer
    queryset = Client.objects.all()


class ProductViewSet(viewsets.ModelViewSet):
    serializer_class = ProductSerializer
    queryset = Product.objects.all()


class PurchaseViewSet(viewsets.ModelViewSet):
    serializer_class = PurchaseSerializer
    queryset = Purchase.objects.all()

    def list(self, request):
        client_id = request.GET.get('client')
        if client_id:
            queryset = Purchase.objects.filter(client__id=client_id)
            serializer = self.serializer_class(queryset, many=True)
            return Response(data=serializer.data)
        else:
            return super().list(request)
    
    def create(self, request):
        data = request.data
        client = Client.objects.get(id=data['client'])
        product = Product.objects.get(id=data['product'])
        amount = data['amount']
        purchase = Purchase(
            client=client,
            product=product,
            amount=amount
        )
        purchase.save()
        serializer = self.serializer_class(purchase)
        headers = self.get_success_headers(serializer.data)
        return Response(
            serializer.data, 
            status=status.HTTP_201_CREATED,
            headers=headers
        )
