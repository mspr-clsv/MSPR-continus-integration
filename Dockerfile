FROM python:alpine3.11
WORKDIR /usr/src
COPY . .
RUN pip3 install -r requirements.txt
RUN python3 manage.py makemigrations app && python3 manage.py migrate app
RUN python3 manage.py makemigrations && python3 manage.py migrate
CMD python3 manage.py runserver 0.0.0.0:$PORT
EXPOSE $PORT